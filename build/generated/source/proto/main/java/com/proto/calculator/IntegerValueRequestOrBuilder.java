// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: calculator/calculator.proto

package com.proto.calculator;

public interface IntegerValueRequestOrBuilder extends
    // @@protoc_insertion_point(interface_extends:calculator.IntegerValueRequest)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <code>int32 number = 1;</code>
   * @return The number.
   */
  int getNumber();
}
