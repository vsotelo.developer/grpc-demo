package com.vsotelo.gprc.server;

import com.proto.calculator.*;
import io.grpc.Status;
import io.grpc.stub.StreamObserver;

import java.util.ArrayList;
import java.util.List;

public class CalculatorServiceImpl extends CalculatorServiceGrpc.CalculatorServiceImplBase {

    @Override
    public void sum(SumRequest request, StreamObserver<SumResponse> responseObserver) {

        responseObserver.onNext(
                SumResponse
                        .newBuilder()
                        .setSumResult(
                                request.getFirstNumber() + request.getSecondNumber()
                        )
                        .build()
        );

        responseObserver.onCompleted();
    }

    @Override
    public void obtainPrimeNumbers(NumberRequest request, StreamObserver<NumberResponse> responseObserver) {

        final long init = request.getInit();
        final long end = request.getEnd();

        if (init > 0 && end > 0 && end > init) {

            List<Long> numbersToEvaluate = new ArrayList<>();

            for (long i = init; i <= end; i++) {
                numbersToEvaluate.add(i);
            }

            numbersToEvaluate.forEach(number -> {
                if (isPrime(number)) {
                    responseObserver.onNext(
                            NumberResponse.newBuilder().setResult(number).build()
                    );
                }
            });

            responseObserver.onCompleted();
        } else {
            responseObserver.onError(new Exception("Check Input Data..!"));
        }
    }

    @Override
    public StreamObserver<OnlyNumberRequest> averageNumbers(StreamObserver<AverageResponse> responseObserver) {
        return new StreamObserver<OnlyNumberRequest>() {

            double quantityNumbers = 0;
            double sumNumbers = 0;

            @Override
            public void onNext(OnlyNumberRequest value) {
                // client send a message
                sumNumbers = sumNumbers + value.getNumber();
                quantityNumbers++;
            }

            @Override
            public void onError(Throwable t) {
            }

            @Override
            public void onCompleted() {
                // client is done
                responseObserver.onNext(
                        AverageResponse.newBuilder()
                                .setResponse(sumNumbers / quantityNumbers)
                                .build()
                );
            }
        };
    }

    @Override
    public StreamObserver<IntegerValueRequest> integerMaxValue(StreamObserver<IntegerValueResponse> responseObserver) {
        System.out.println("called..!");
        return new StreamObserver<IntegerValueRequest>() {

            int maxValue = 0;

            @Override
            public void onNext(IntegerValueRequest value) {
                if (value.getNumber() > maxValue) {

                    maxValue = value.getNumber();
                    // Sending message.!
                    responseObserver.onNext(
                            IntegerValueResponse.newBuilder()
                                    .setResult(maxValue)
                                    .build()
                    );
                }
            }

            @Override
            public void onError(Throwable t) {
            }

            @Override
            public void onCompleted() {
                responseObserver.onCompleted();
            }
        };
    }

    private boolean isPrime(long number) {
        int totalDivisors = 0;

        for (int i = 1; i <= number; i++) {
            if (number % i == 0) {
                totalDivisors++;
            }
        }

        return totalDivisors == 2;
    }

    @Override
    public void squareRoot(SquareRootRequest request, StreamObserver<SquareRootResponse> responseObserver) {

        int number = request.getNumber();

        if (number >= 0) {
            double numberRoot = Math.sqrt(number);
            responseObserver.onNext(
                    SquareRootResponse.newBuilder()
                            .setRootedNumber(numberRoot)
                            .build()
            );
            responseObserver.onCompleted();
        } else {
            // we construct the exception
            responseObserver.onError(
                    Status.INVALID_ARGUMENT
                            .withDescription("The number being send is not positivo")
                            .augmentDescription("Numbers sent: " + number)
                            .asRuntimeException()
            );
        }

    }
}
