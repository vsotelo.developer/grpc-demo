package com.vsotelo.gprc.client;

import com.proto.calculator.*;
import com.proto.greet.*;
import io.grpc.*;
import io.grpc.stub.StreamObserver;

import java.util.Arrays;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class OnClient {

    public static void main(String[] args) {

        System.out.println("Hello I'm a gRPC client");

        ManagedChannel channel = ManagedChannelBuilder
                .forAddress("localhost", 50051)
                .usePlaintext()
                .build();

        /*
        System.out.println("*** gRPC Greet ****");
        processRPCGreet(channel);

        System.out.println("*** gRPC Calculator ****");
        processRPCCalculator(channel);

        System.out.println("*** gRPC Streaming Greetings ****");
        processServerStreaming(channel);

        System.out.println("*** gRPC Streaming Numbers Prime ****");
        processServerStreamingPrime(channel);

        System.out.println("*** gRPC Streaming Client Average ****");
        processAverageNumbers(channel);

        System.out.println("*** gRPC Streaming BI ****");
        processStreamingBi(channel);

        System.out.println("*** gRPC Streaming BI Max Value ****");
        processStreamingBiMaxValues(channel);

        System.out.println("*** gRPC Error ****");
        doErrorCall(channel);
        */

        System.out.println("*** gRPC call with deadline ****");
        doCallWithDeadline(channel);


        System.out.println("Shutting down channel");
        channel.shutdown();

    }

    // Unit
    static void processRPCGreet(ManagedChannel channel) {
        // created a greet service client (blocking - synchronous)
        GreetServiceGrpc.GreetServiceBlockingStub greetClient
                = GreetServiceGrpc.newBlockingStub(channel);

        // created a protocol buffer greeting message
        Greeting greeting = Greeting.newBuilder()
                .setFirstName("Victor")
                .setLastName("Sotelo")
                .build();

        // do the same for a GreetRequest
        GreetRequest request = GreetRequest.newBuilder()
                .setGreeting(greeting)
                .build();

        // call the RPC and get back a GreetResponse (protocol buffers)
        GreetResponse greetResponse = greetClient.greet(request);

        System.out.println(greetResponse.getResult());

        // do something

    }

    // Unit
    static void processRPCCalculator(ManagedChannel channel) {

        CalculatorServiceGrpc.CalculatorServiceBlockingStub calculatorClient
                = CalculatorServiceGrpc.newBlockingStub(channel);

        SumResponse sumResponse = calculatorClient.sum(
                SumRequest.newBuilder()
                        .setFirstNumber(10)
                        .setSecondNumber(10)
                        .build()
        );

        System.out.println(sumResponse.getSumResult());
    }

    // Server Streaming
    static void processServerStreaming(ManagedChannel channel) {

        // created a greet service client (blocking - synchronous)
        GreetServiceGrpc.GreetServiceBlockingStub greetClient
                = GreetServiceGrpc.newBlockingStub(channel);

        // we prepare streaming
        GreetManyTimesRequest greetManyTimesRequest = GreetManyTimesRequest.newBuilder()
                .setGreeting(Greeting.newBuilder().setFirstName("Victor").build())
                .build();

        // we stream the response (in a blocking manner)
        greetClient.greetManyTimes(greetManyTimesRequest)
                .forEachRemaining(response -> {
                    System.out.println(response.getResult());
                });

    }

    // Server Streaming
    static void processServerStreamingPrime(ManagedChannel channel) {

        CalculatorServiceGrpc.CalculatorServiceBlockingStub calculatorClient
                = CalculatorServiceGrpc.newBlockingStub(channel);

        NumberRequest numberRequest = NumberRequest.newBuilder()
                .setInit(1)
                .setEnd(100)
                .build();

        calculatorClient.obtainPrimeNumbers(numberRequest)
                .forEachRemaining(numberResponse -> {
                    System.out.println(numberResponse.getResult());
                });

    }

    // Client Streaming
    static void processClientStreamingGreet(ManagedChannel channel) {

        // create an asynchronous client
        GreetServiceGrpc.GreetServiceStub asyncClient
                = GreetServiceGrpc.newStub(channel);

        CountDownLatch countDownLatch = new CountDownLatch(1);

        StreamObserver<LongGreetRequest> requestObserver = asyncClient.longGreet(new StreamObserver<LongGreetResponse>() {

            @Override
            public void onNext(LongGreetResponse value) {
                // we get response from the server
                System.out.println("Received a response from the server");
                System.out.println(value.getResult());
            }

            @Override
            public void onError(Throwable t) {
                // we get an error from the server
            }

            @Override
            public void onCompleted() {
                // the server is done sending us data
                // on completed will be called right after onNext()
                System.out.println("Server has completed sending us something");
                countDownLatch.countDown();
            }
        });

        // streaming message #1
        System.out.println("sending message 1");
        requestObserver.onNext(LongGreetRequest.newBuilder()
                .setGreeting(Greeting.newBuilder()
                        .setFirstName("Victor")
                        .build())
                .build());

        // streaming message #2
        System.out.println("sending message 2");
        requestObserver.onNext(LongGreetRequest.newBuilder()
                .setGreeting(Greeting.newBuilder()
                        .setFirstName("John")
                        .build())
                .build());

        // streaming message #3
        System.out.println("sending message 3");
        requestObserver.onNext(LongGreetRequest.newBuilder()
                .setGreeting(Greeting.newBuilder()
                        .setFirstName("Marc")
                        .build())
                .build());

        // whe tell the server that the client is done sending data
        requestObserver.onCompleted();

        try {
            countDownLatch.await(3L, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    // Client Streaming
    static void processAverageNumbers(ManagedChannel channel) {

        // create an asynchronous client
        CalculatorServiceGrpc.CalculatorServiceStub asyncClient
                = CalculatorServiceGrpc.newStub(channel);

        CountDownLatch countDownLatch = new CountDownLatch(1);

        StreamObserver<OnlyNumberRequest> requestObserver = asyncClient.averageNumbers(new StreamObserver<AverageResponse>() {

            @Override
            public void onNext(AverageResponse value) {
                // we get response from the server
                System.out.println("Received a response from the server");
                System.out.println(value.getResponse());
            }

            @Override
            public void onError(Throwable t) {
            }

            @Override
            public void onCompleted() {
                System.out.println("Server has completed sending us something");
                countDownLatch.countDown();
            }
        });

        // streaming message #1
        System.out.println("sending message 1");
        requestObserver.onNext(OnlyNumberRequest.newBuilder()
                .setNumber(1)
                .build());

        // streaming message #2
        System.out.println("sending message 2");
        requestObserver.onNext(OnlyNumberRequest.newBuilder()
                .setNumber(2)
                .build());

        // streaming message #3
        System.out.println("sending message 3");
        requestObserver.onNext(OnlyNumberRequest.newBuilder()
                .setNumber(3)
                .build());

        // streaming message #4
        System.out.println("sending message 4");
        requestObserver.onNext(OnlyNumberRequest.newBuilder()
                .setNumber(4)
                .build());

        // whe tell the server that the client is done sending data
        requestObserver.onCompleted();

        try {
            countDownLatch.await(3L, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    // Bi Streaming
    static void processStreamingBi(ManagedChannel channel) {

        GreetServiceGrpc.GreetServiceStub asyncClient
                = GreetServiceGrpc.newStub(channel);

        CountDownLatch countDownLatch = new CountDownLatch(1);

        StreamObserver<GreetEveryoneRequest> requestObserver = asyncClient.greetEveryone(new StreamObserver<GreetEveryoneResponse>() {

            @Override
            public void onNext(GreetEveryoneResponse value) {
                System.out.println("Response from server" + value.getResult());
            }

            @Override
            public void onError(Throwable t) {
                countDownLatch.countDown();
            }

            @Override
            public void onCompleted() {
                System.out.println("Server is done sending data");
                countDownLatch.countDown();
            }
        });

        Arrays.asList("Stephan", "John", "Marc", "Patricia").forEach(
                name -> requestObserver.onNext(GreetEveryoneRequest.newBuilder()
                        .setGreeting(Greeting.newBuilder()
                                .setFirstName(name)
                                .build())
                        .build())
        );

        requestObserver.onCompleted();

        try {
            countDownLatch.await(3, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    // Bi Streaming
    static void processStreamingBiMaxValues(ManagedChannel channel) {

        CalculatorServiceGrpc.CalculatorServiceStub asyncClient
                = CalculatorServiceGrpc.newStub(channel);

        CountDownLatch countDownLatch = new CountDownLatch(1);

        StreamObserver<IntegerValueRequest> requestObserver = asyncClient.integerMaxValue(new StreamObserver<IntegerValueResponse>() {

            @Override
            public void onNext(IntegerValueResponse value) {
                System.out.println("Receiving response from server..." + value.getResult());
            }

            @Override
            public void onError(Throwable t) {
                countDownLatch.countDown();
            }

            @Override
            public void onCompleted() {
                System.out.println("Server is done sending data...");
                countDownLatch.countDown();
            }
        });

        Arrays.asList(1, 5, 3, 6, 2, 20).forEach(
                number -> requestObserver.onNext(IntegerValueRequest.newBuilder()
                        .setNumber(number)
                        .build())
        );

        requestObserver.onCompleted();

        try {
            countDownLatch.await(3, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    // On errors

    // Unit
    static void doErrorCall(ManagedChannel channel) {

        CalculatorServiceGrpc.CalculatorServiceBlockingStub blockingStub
                = CalculatorServiceGrpc.newBlockingStub(channel);

        int number = -1;

        try {

            SquareRootResponse response = blockingStub.squareRoot(SquareRootRequest.newBuilder()
                    .setNumber(number)
                    .build());

            System.out.println(response.getRootedNumber());

        } catch (StatusRuntimeException e) {
            System.out.println("Go an exception for square root!");
            e.printStackTrace();
        }

    }

    static void doCallWithDeadline(ManagedChannel channel) {

        GreetServiceGrpc.GreetServiceBlockingStub blockingStub
                = GreetServiceGrpc.newBlockingStub(channel);

        // first call (3000 ms deadline)
        try {
            System.out.println("Sending a request with deadline of 3000 ms");
            GreetWithDeadlineResponse response = blockingStub.withDeadline(Deadline.after(3000, TimeUnit.MILLISECONDS))
                    .greetWithDeadline(
                            GreetWithDeadlineRequest.newBuilder()
                                    .setGreeting(Greeting.newBuilder()
                                            .setFirstName("Victor")
                                            .build())
                                    .build()
                    );

            System.out.println(response.getResult());

        } catch (StatusRuntimeException e) {
            if (e.getStatus() == Status.DEADLINE_EXCEEDED) {
                System.out.println("Deadline has been exceeded, wo don't want the response");
            } else {
                e.printStackTrace();
            }
        }

        // second call (100 ms deadline)
        try {
            System.out.println("Sending a request with deadline of 100 ms");
            GreetWithDeadlineResponse response = blockingStub.withDeadline(Deadline.after(100, TimeUnit.MILLISECONDS))
                    .greetWithDeadline(
                            GreetWithDeadlineRequest.newBuilder()
                                    .setGreeting(Greeting.newBuilder()
                                            .setFirstName("Melissa")
                                            .build())
                                    .build()
                    );

            System.out.println(response.getResult());

        } catch (StatusRuntimeException e) {
            if (e.getStatus() == Status.DEADLINE_EXCEEDED) {
                System.out.println("Deadline has been exceeded, wo don't want the response");
            } else {
                e.printStackTrace();
            }
        }

    }
}